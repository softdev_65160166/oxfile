/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author aotto
 */
public class TestWriteFriend {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Friend f1 = new Friend("Pom",70,"0811111111");
        Friend f2 = new Friend("Poom",50,"0811111112");
        File file = new File("friends.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(f1);
        oos.writeObject(f2);
        oos.close();
        fos.close();
    }
}
